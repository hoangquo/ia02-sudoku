Projet Sudoku Solver en Prolog
===

Ce projet Sudoku Solver a été réalisé dans le cadre de l'**UV IA02** (Résolution de problèmes et programmation logique).
Les codes disponibles sur ce repo permettent de générer une grille de sudoku
(avec différents niveaux de difficultés), de compléter manuellement la grille et
de la résoudre à l'aide d'une _IA symbolique_ que nous avons développée. Il y a 
également de fonctionnalités supplémentaires telles que l'importation et
l'exportation de grilles.

Pour comprendre, le fonctionnement du programme ainsi que ses fonctionnalités
nous vous invitons à lire le **rapport** que vous trouverez sur ce repo.

# Auteurs
Quoc Trung Hoang et Marc Damie

# Remarque
Lors de ce projet, nous n'avions pas le droit d'utiliser la programmation par contraintes.

# Crédits pour le logo
Flusjaa : https://www.deviantart.com/flusjaa/art/FREE-Avatar-Sudoku-378369960
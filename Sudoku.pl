% working_directory(_,'/Users/hoangquoctrung/Desktop/IA02/TP1').
% load_files('Sudoku.pl',[encoding(utf8)]).
:- load_files('Generate.pl',[encoding(utf8)]).
:- set_prolog_stack(global, limit(100 000 000 000 000)).


% On verifie si la grille est complète
fullLine([]).
fullLine([0|_]):-               !,fail.
fullLine([_|Q]):-               fullLine(Q).

fullGrid([]).
fullGrid([T|Q]):-               fullLine(T), fullGrid(Q).

% player move
getPlayerMove(B,Res):-          write('Entrez le numero de la ligne'),nl,
                                read(I), ind(I),
                                write('Entrez le numero de la colonne'),nl,
                                read(J), ind(J),
                                write('Entrez la valeur à insérer'),nl,
                                read(Val), num(Val),
                                insertMove(I,J,Val,B,Res).

insertMove(I,J,Val, B,Res):-    sol_unique(I,J,B,Val),
                                insertInBoard(Val,I,J,B,Res),
                                setMoveHistory(I,J,Val),
                                write('Valeur ajoutée'),nl,!.

insertMove(I,J,_, B,B):-        \+element(I,J,B,0),!,
                                write('Case déjà remplie'),nl.
insertMove(_,_,_,B,B):-         write('Valeur non valide'),nl.



% old moves lists

replaceDynamicBoard(Bf):-       retractall(board(_)), asserta(board(Bf)).

setMoveHistory(X,Y,Val):-       board(B),
                                element(X,Y,B,OldVal),
                                asserta(move(X,Y,Val,OldVal)).

retractLastMove():-             \+ move(_,_,_,_),
                                write('Aucun dernier mouvement'),nl,!.
retractLastMove():-             board(B),
                                move(X,Y,Val,OldVal),
                                insertInBoard(OldVal,X,Y,B,Res),
                                replaceDynamicBoard(Res),
                                retract(move(X,Y,Val,OldVal)),
                                write('Grille obtenue:'),nl,
                                showBoard(Res),!.

% menu

startGameManager():-            init(),
                                boucle_menu(),
                                initMoves(),
                                initSols().


boucle_menu():- repeat, menu, !.
menu():-
          nl, write('1. Generer une grille à remplir'),nl,
          write('2. Remplir une case'),nl,
          write('3. Retour en arriere'),nl,
          write('4. Générer une ou plusieurs solutions'),nl,
          write('5. Afficher simplement la grille'),nl,
          write('6. Réinitialiser la grille'),nl,
          write('7. Obtenir la solution d''une case'),nl,
          write('8. Lire une grille a partir du fichier Board.txt'),nl,
          write('9. Exporter la grille actuelle'),nl,
          write('10. Terminer'),nl,
          write('Entrer un choix '),nl,
          read(Choix),nl,
          appel(Choix),
          Choix=10, nl.


appel(1):-                      write('Veuillez choisir un niveau de difficulté'),nl,
                                write('1. Très facile'),nl,
                                write('2. Facile'),nl,
                                write('3. Moyen'),nl,
                                write('4. Difficile'),nl,
                                write('Par défaut : Facile'),nl,
                                read(Choix),nl,
                                initMoves(),
                                choixDifficulte(Choix),nl,!.

appel(2):-                      write('Grille:'),
                                nl,board(B),
                                showBoard(B),
                                getPlayerMove(B,Res),
                                replaceDynamicBoard(Res),nl,
                                write('Grille obtenue:'),nl,
                                showBoard(Res),
                                gameWon(Res),!.

appel(3):-                      retractLastMove(),!.

appel(4):-             asserta(boardSol(0)),board(B),
                                write('Combien de solutions souhaitez-vous voir?'),nl,
                                read(Choix),
                                boucle_solve(Choix,B),!.
appel(5):-                      board(B),
                                showBoard(B),!.
appel(6):-                      init,initMoves(),!.
appel(7):-                      board(B),showBoard(B),nl,
                                write('Entrez le numero de la ligne'),nl,
                                read(I), ind(I),
                                write('Entrez le numero de la colonne'),nl,
                                read(J), ind(J),
                                solveCell(I,J,B,Res),
                                replaceDynamicBoard(Res),!.

%appel(8):-                      write('Entrez le nom du fichier'),nl,
%                                read(Name),
%                                \+ readBoard(_,Name),
%                                write('Fichier introuvable'),!,nl.

appel(8):-                      \+ exists_file('Board.txt'),write('Fichier Board.txt introuvable'),!.

appel(8):-                      write('Importation du fichier Board.txt'),nl,
                                readFile(Res),
                                replaceDynamicBoard(Res),
                                showBoard(Res),
                                initMoves,
                                write('Importation réussie'),!,nl.


appel(9):-                      board(B),ecrire(B),nl,write('Board exporté dans Board.txt'),!.

appel(10):-                     write('Au revoir'),!.

appel(_):-                      write('Vous avez mal choisi').


choixDifficulte(1):-            genValidBoard(B),
                                solve(B,Prerendered),
                                generateBoardExtremelyEasy(Prerendered,Rendered),
                                replaceDynamicBoard(Rendered),showBoard(Rendered),!.


choixDifficulte(3):-            genValidBoard(B),
                                solve(B,Prerendered),
                                generateBoardMedium(Prerendered,Rendered),solve(Rendered,_),
                                replaceDynamicBoard(Rendered),showBoard(Rendered),!.

choixDifficulte(4):-            genValidBoard(B),
                                solve(B,Prerendered),
                                generateBoardHard(Prerendered,Rendered),solve(Rendered,_),
                                replaceDynamicBoard(Rendered),showBoard(Rendered),!.

choixDifficulte(_):-            genValidBoard(B),
                                solve(B,Prerendered),
                                generateBoardEasy(Prerendered,Rendered),
                                replaceDynamicBoard(Rendered),showBoard(Rendered).

countCellLine(0,[]).
countCellLine(N,[0|Q]):- countCellLine(N,Q),!.
countCellLine(N,[_|Q]):- countCellLine(N1,Q),N is N1+1.

countCellBoard(0,[]).
countCellBoard(N,[T|Q]):- countCellLine(N1,T),countCellBoard(N2,Q),N is N1+N2.

genValidBoard(B):- emptyBoard(Vide), genValidBoard(5,Vide,B).
genValidBoard(0,B,B):-!.
genValidBoard(N,B,Res):- N1 is N-1,genValidBoard(N1,B,Res1), getRandomCouple(I,J), element(I,J,Res1,0),random_between(1,9,Val), insertInBoard(Val,I,J,Res1,Res), isValidBoard(Res).

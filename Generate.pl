% working_directory(_,'/Users/hoangquoctrung/Desktop/IA02/TP1').
% load_files('Generate.pl',[encoding(utf8)]).
% testBoard3(B), solve_line_Board(1,B,R), showBoard(R), getRandomCouple(I,J), preRenderBoardMedium(I,J,R,Res,10), showBoard(Res).
% testBoard3(B), solve(B,R), showBoard(R), renderBoardMedium(R,Res),showBoard(Res).
use_module(library(random)).


getHardExecutionTime(ExecutionTime):-           statistics(walltime, [_ | [_]]),
                                                testBoard(B), solve(B,R), generateBoardHard(R,_),
                                                statistics(walltime, [_ | [ExecutionTime]]),
                                                write('Execution Time '), write(ExecutionTime), write('ms'),nl.

getAverageHardExecutionTime(Avg,N,M,Res):-      N < M,
                                                TempN is N+1,
                                                getHardExecutionTime(ExecutionTime),
                                                NewAvg is Avg + ExecutionTime,
                                                getAverageHardExecutionTime(NewAvg,TempN,M,Res).
getAverageHardExecutionTime(Avg,N,N,Res):-      Res is Avg/N,!.
getAverageHardExecutionTime(Avg,N):-            getAverageHardExecutionTime(0,0,N,Avg),!.



getExtremelyEasyExecutionTime(ExecutionTime):-  statistics(walltime, [_ | [_]]),
                                                testBoard(B), solve(B,R), generateBoardExtremelyEasy(R,_),
                                                statistics(walltime, [_ | [ExecutionTime]]),
                                                write('Execution Time '), write(ExecutionTime), write('ms'),nl.



getAverageExtremelyEasyExecutionTime(Avg,N,M,Res):-     N < M,
                                                        TempN is N+1,
                                                        getExtremelyEasyExecutionTime(ExecutionTime),
                                                        NewAvg is Avg + ExecutionTime,
                                                        getAverageExtremelyEasyExecutionTime(NewAvg,TempN,M,Res).
getAverageExtremelyEasyExecutionTime(Avg,N,N,Res):-     Res is Avg/N,!.
getAverageExtremelyEasyExecutionTime(Avg,N):-           getAverageExtremelyEasyExecutionTime(0,0,N,Avg),!.


getMediumExecutionTime(ExecutionTime):-             statistics(walltime, [_ | [_]]),
                                                    testBoard(B), solve(B,R), generateBoardMedium(R,_),
                                                    statistics(walltime, [_ | [ExecutionTime]]),
                                                    write('Execution Time '), write(ExecutionTime), write('ms'),nl.

getAverageMediumExecutionTime(Avg,N,M,Res):-        N < M,
                                                    TempN is N+1,
                                                      getMediumExecutionTime(ExecutionTime),
                                                    NewAvg is Avg + ExecutionTime,
                                                    getAverageMediumExecutionTime(NewAvg,TempN,M,Res).
getAverageMediumExecutionTime(Avg,N,N,Res):-        Res is Avg/N,!.
getAverageMediumExecutionTime(Avg,N):-              getAverageMediumExecutionTime(0,0,N,Avg),!.

testBoard([
  [_,7,_,1,_,_,9,_,3],
  [8,_,1,_,_,6,_,7,_],
  [_,9,_,_,3,_,8,_,5],
  [7,_,_,_,_,5,_,4,_],
  [_,_,8,_,_,_,7,_,_],
  [_,6,_,8,_,_,_,_,9],
  [1,_,3,_,9,_,_,5,_],
  [_,5,_,6,_,_,4,_,1],
  [9,_,6,_,_,7,_,8,_]
]).



emptyBoard([
  [_,_,_,_,_,_,_,_,_],
  [_,_,_,_,_,_,_,_,_],
  [_,_,_,_,_,_,_,_,_],
  [_,_,_,_,_,_,_,_,_],
  [_,_,_,_,_,_,_,_,_],
  [_,_,_,_,_,_,_,_,_],
  [_,_,_,_,_,_,_,_,_],
  [_,_,_,_,_,_,_,_,_],
  [_,_,_,_,_,_,_,_,_]
]).

testBoard2([
  [4,_,_,1,_,_,_,_,3],
  [_,_,_,_,5,_,2,_,4],
  [6,9,_,_,3,_,8,_,5],
  [7,_,_,3,_,_,1,4,_],
  [5,_,_,_,_,_,7,_,_],
  [_,6,_,_,7,_,_,_,9],
  [1,_,_,_,_,_,6,_,7],
  [_,5,_,_,_,_,4,9,1],
  [_,_,_,_,_,_,_,8,_]
]).

num(1).
num(2).
num(3).
num(4).
num(5).
num(6).
num(7).
num(8).
num(9).

ind(9).
ind(1).
ind(2).
ind(3).
ind(4).
ind(5).
ind(6).
ind(7).
ind(8).

init():-    retractall((board(_))),
            asserta((board(
                  [
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_]
                  ]
                ))).

initMoves():-   retractall((move(_,_,_,_))).
initSols():-    asserta((boardSol(
                  [
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_],
                    [_,_,_,_,_,_,_,_,_]
                  ]
                ))),
                retractall((boardSol(_))).

% concatenation de L1 et L2 dans L3
concat([],L2,L2).
concat([T|Q],L,[T|R]):- concat(Q,L,R).


% retourne vrai si element in L et N son indice
element(0,[0|_],1):-    !.
element(X,[X|_],1):-    !.
element(X,[_|Q],N):-    TempN is N-1,
                        element(X,Q,TempN).


element(I,J,B,X):-      element(L,B,I),
                        element(X,L,J).



% affichage de la matrice


%getVal
getVal(0,0):-           !.
getVal(T,T).

% affichage d''une ligne
showLine([],9):-        write(' │ '),nl,!.
showLine([T|Q],N):-     0 is mod(N,3),
                        write(' | '),
                        getVal(T,Val),
                        write(Val),
                        TempN is N+1,
                        showLine(Q,TempN),!.

showLine([T|Q],N):-     write('  '),
                        getVal(T,Val),
                        write(Val),
                        TempN is N+1,
                        showLine(Q,TempN),!.

% affichage des dividers
drawRow(0):-            write(''),nl,!.
drawRow(9):-            write(''),nl,!.
drawRow(M):-            0 is mod(M,3),
                        write(''),nl.


% affichage en entier du board
showBoard(B):-          showBoard(B,0).
showBoard([],9):-       drawRow(9),!.
showBoard([T|Q],M):-    M < 9,
                        0 is mod(M,3),
                        drawRow(M),
                        showLine(T,0),
                        TempM is 1+M,
                        showBoard(Q,TempM),!.
showBoard([T|Q],M):-    showLine(T,0),
                        TempM is 1+M,
                        showBoard(Q,TempM).




% predicat different
different(0,_):-        !.
different(_,[]):-       !.
different(A,[0|Q]):-    different(A,Q),!.
different(A,[T|Q]):-    A \= T,
                        different(A,Q).


% validite d''une ligne
isValidLine([]).
isValidLine([T|Q]):-    different(T,Q),
                        isValidLine(Q).

% validite des lignes
isValidLines([]).
isValidLines([L1|L2]):- isValidLine(L1),
                        isValidLines(L2).

% validite des colonnes
isValidCols([[],[],[],[],[],[],[],[],[]]):- !.
isValidCols([[T1|Q1],[T2|Q2],[T3|Q3],[T4|Q4],[T5|Q5],[T6|Q6],[T7|Q7],[T8|Q8],[T9|Q9]]):-
                                            isValidLine([T1,T2,T3,T4,T5,T6,T7,T8,T9]),
                                            isValidCols([Q1,Q2,Q3,Q4,Q5,Q6,Q7,Q8,Q9]).

% flatten col
colToLine(_,[],[]).
colToLine(J,[T|Q],Res):-                    colToLine(J,Q,R),
                                            element(X,T,J),
                                            concat([X],R,Res),!.


% flatten square
squareToLine(I,J,B,[A11,A12,A13,A21,A22,A23,A31,A32,A33]):-
                                            I1 is div(I-1,3)*3+1,
                                            I2 is I1+1,
                                            I3 is I1+2,
                                            J1 is div(J-1,3)*3+1,
                                            J2 is J1+1,
                                            J3 is J1+2,
                                            element(I1,J1,B,A11),
                                            element(I1,J2,B,A12),
                                            element(I1,J3,B,A13),
                                            element(I2,J1,B,A21),
                                            element(I2,J2,B,A22),
                                            element(I2,J3,B,A23),
                                            element(I3,J1,B,A31),
                                            element(I3,J2,B,A32),
                                            element(I3,J3,B,A33).


% validite d''un carre 3x3
isValidSquare([L1,L2,L3]):-                 concat(L1,L2,Temp),
                                            concat(Temp,L3,L4),
                                            isValidLine(L4).

% validite du carre suivant
isValidSquares([[],[],[]]):-                !.
isValidSquares([[A11,A12,A13|Next1],[A21,A22,A23|Next2],[A31,A32,A33|Next3]|_]):-
                                isValidSquare([[A11,A12,A13],[A21,A22,A23],[A31,A32,A33]]),
                                isValidSquares([Next1,Next2,Next3]).



% validite d''une grille
isValidGrid([]):-                           !.
isValidGrid([L1,L2,L3|Next]):-              isValidSquares([L1,L2,L3]),isValidGrid(Next).



% Fonction qui verifie que le Board est valide
isValidBoard([]).
isValidBoard(B):-                           isValidLines(B),
                                            isValidCols(B),
                                            isValidGrid(B).



% Inserer dans une ligne
insertInLine(X,1,[],[X]):-                  !.
insertInLine(X,1,[_|Q],[X|Q]):-             !.
insertInLine(X,N,[T|Q],[T|Res]):-           N1 is N-1,
                                            insertInLine(X,N1,Q,Res).




% Inserer une valeur dans un board
insertInBoard(X,I,J,B,Res):-                element(L,B,I),
                                            insertInLine(X,J,L,Resline),
                                            insertInLine(Resline,I,B,Res).


% supprimer dans une ligne
dropInLine(1,[],[_]):-                      !.
dropInLine(N,[T|Q],[T|Res]):-               N1 is N-1,
                                            dropInLine(N1,Q,Res).


% generation d''une solution unique sur une ligne
sol_unique_ligne(I,B,Val):-                 num(Val),
                                            ind(I),
                                            element(L,B,I),
                                            different(Val,L).





% generation d''une solution unique sur une colonne
sol_unique_col(J,B,Val):-                   num(Val),
                                            ind(J),
                                            colToLine(J,B,L),
                                            different(Val,L).




% generation d''une solution unique sur un 3x3
sol_unique_square(I,J,B,Val):-              ind(J),!,
                                            ind(I),!,
                                            num(Val),
                                            squareToLine(I,J,B,L),
                                            different(Val,L).


% generation d''une solution unique pour une cellule a la ieme ligne et jeme col
sol_unique(I,J,B,Val):-                     num(Val),
                                            sol_unique_ligne(I,B, Val),
                                            sol_unique_col(J,B, Val),
                                            sol_unique_square(I,J,B,Val).


% valeur unique
valUnique(Val,L):-                          num(Val),
                                            different(Val,L).

% resolution de la grille
solve_line(_,10,B,B):-                      !.
solve_line(I,J,B,Res):-                     element(I,J,B,0),
                                            J1 is J+1,
                                            solve_line(I,J1,B,Res1),
                                            sol_unique(I,J,Res1,Val),
                                            insertInBoard(Val, I,J, Res1,Res).

solve_line(I,J,B,Res):-                     \+element(I,J,B,0),
                                            J1 is J+1,
                                            solve_line(I,J1,B,Res).

solve_line_Board(10,B,B):-!.
solve_line_Board(I,B,Res):-                 I1 is I+1,
                                            solve_line_Board(I1,B,Res1),
                                            solve_line(I,1,Res1,Res).

solve(B,_):- \+ isValidGrid(B),!,fail. %on vérifie la première fois si la grille qu''on nous envoie est valide
solve(B,Res):-                              solve_line_Board(1,B,Res).

%Permet de regarder les differentes solutions

boucle_solve(0,_):- retractall(boardSol(_)),!.
boucle_solve(I,B):- solve(B,Res), \+boardSol(Res), showBoard(Res), asserta(boardSol(Res)), I1 is I-1,write('Entrer ok pour la suite'),nl,read(_), boucle_solve(I1,B),!.
boucle_solve(_,_):- nl,write('Pas d''autre solution'),nl,retractall(boardSol(_)).


%résolution d''une case, on génère une solution car un mouvement meme valide mene parfois à des grilles insolvables
solveCell(I,_,_,_):- \+ind(I), write('Case invalide'),!.
solveCell(_,J,_,_):- \+ind(J), write('Case invalide'),!.
solveCell(I,J,B,B):- \+element(I,J,B,0), write('Case déjà remplie'),!.
solveCell(I,J,B,Res):- solve(B,ResTemp), element(I,J,ResTemp,X), insertInBoard(X,I,J,B,Res),showBoard(Res).



% grading simple and extremely simple board
% sequencing digging holes (Global randomising)
getRandomCouple(I,J):-                      repeat,
                                            random_between(1,9,I),
                                            random_between(1,9,J).

removeRandom(B,Res):-                       getRandomCouple(I,J),
                                            \+ element(I,J,B,0),
                                            insertInBoard(_,I,J,B,Res).

% prerendering the board
preRenderBoardEasy(B,B,0).
preRenderBoardEasy(B,Res,N):-               removeRandom(B,TempRes),
                                            TempN is N-1,
                                            solve_line_Board(1,B,_),
                                            preRenderBoardEasy(TempRes,Res,TempN),!.


generateBoardEasy(B,Res):-                  random_between(32,45,N),
                                            preRenderBoardEasy(B,Res,N).

generateBoardExtremelyEasy(B,Res):-         random_between(10,31,N),
                                            preRenderBoardEasy(B,Res,N).





% grading medium ones
% sequencing digging holes (Cell Jumping)
getRandomDirection(D):-                     repeat,
                                            random_between(1,8,D).
% I -> Ligne, J -> Col
% 1 -> left
% 2 -> upperleft
% 3 -> up
% 4 -> upperright
% 5 -> right
% 6 -> lowerright
% 7 -> down
% 8 -> lowerleft

%left
getTranslation(I,J,1,I,NewJ):-              NewJ is J-1,
                                            NewJ > 0,!.
%upper left
getTranslation(I,J,2,NewI,NewJ):-           NewI is I-1,
                                            NewI > 0,
                                            NewJ is J-1,
                                            NewJ > 0,!.
%up
getTranslation(I,J,3,NewI,J):-              NewI is I-1,
                                            NewI > 0,!.
%upper right
getTranslation(I,J,4,NewI,NewJ):-           NewI is I-1,
                                            NewI > 0,
                                            NewJ is J+1,
                                            NewJ < 9,!.
%right
getTranslation(I,J,5,I,NewJ):-              NewJ is J+1,
                                            NewJ < 9,!.
%lower right
getTranslation(I,J,6,NewI,NewJ):-           NewI is I+1,
                                            NewI < 9,
                                            NewJ is J+1,
                                            NewJ < 9,!.
%down
getTranslation(I,J,7,NewI,J):-              NewI is I+1,
                                            NewI < 9,!.
%lower left
getTranslation(I,J,8,NewI,NewJ):-           NewI is I+1,
                                            NewI < 9,
                                            NewJ is J-1,
                                            NewJ > 0,!.

removeCellJump(I,J,B,B):-                   element(I,J,B,_),
                                            insertInBoard(_,I,J,B,Res),
                                            \+ solve_line_Board(1,Res,_),!.
removeCellJump(I,J,B,Res):-                 insertInBoard(_,I,J,B,Res),!.
removeCellJump(_,_,B,Res):-                 getRandomCouple(NewI,NewJ),
                                            removeCellJump(NewI,NewJ,B,Res).

preRenderBoardMedium(_,_,B,B,0).
preRenderBoardMedium(I,J,B,Res,N):-         removeCellJump(I,J,B,TempRes),
                                            TempN is N-1,
                                            solve_line_Board(1,B,_),
                                            getRandomDirection(D),
                                            getTranslation(I,J,D,NewI,NewJ),
                                            preRenderBoardMedium(NewI,NewJ,TempRes,Res,TempN),!.

renderBoardMedium(B,Res):-                  random_between(46,49,N),
                                            getRandomCouple(I,J),
                                            preRenderBoardMedium(I,J,B,Res,N).

getPropagationMethodMedium(Val):-           repeat, random_between(1,3,Val).
generateBoardMedium(B,Res):-                renderBoardMedium(B,RenderedRes),
                                            getPropagationMethodMedium(Val),
                                            setPropagationMethod(Val,RenderedRes,Res).


% grading hard ones
preRenderBoardHard(_,_,B,B,0).
preRenderBoardHard(I,J,B,Res,N):-           removeCellJump(I,J,B,TempRes),
                                            TempN is N-1,
                                            solve_line_Board(1,B,_),
                                            getRandomDirection(D),
                                            getTranslation(I,J,D,NewI,NewJ),
                                            preRenderBoardHard(NewI,NewJ,TempRes,Res,TempN),!.


renderBoardHard(B,Res):-                    random_between(50,53,N),
                                            getRandomCouple(I,J),
                                            preRenderBoardHard(I,J,B,Res,N).

getPropagationMethodHard(Val):-             repeat, random_between(1,3,Val).
generateBoardHard(B,Res):-                  renderBoardHard(B,RenderedRes),
                                            getPropagationMethodHard(Val),
                                            setPropagationMethod(Val,RenderedRes,Res).


% get propagation method
setPropagationMethod(1,B,Res):-             rotateCols(1,B,Res).
setPropagationMethod(2,B,Res):-             random_between(0,2,Val),
                                            Lower is (Val * 3)+ 1,
                                            Upper is Lower + 2,
                                            swapColumn(Lower,Upper,B,Res).
setPropagationMethod(3,B,Res):-             swapSquare(B,Res).
setPropagationMethod(Val,B,B):-             Val > 3,!.


% propagation par rotation (grid rolling)
rotateCol(_,[],[]).
rotateCol(J,[T|Q],Res):-                    rotateCol(J,Q,R),
                                            element(X,T,J),
                                            concat(R,[X],Res).


rotateCols(8,_,_).
rotateCols(J,B,Res):-                       rotateCol(J,B,R),
                                            TempJ is J+1,
                                            rotateCols(TempJ,B,TempRes),
                                            concat([R],TempRes,Res),!.


% propagation par echange de colonnes
insertInColumn(_,_,[],Res,Res).
insertInColumn(_,10,_,_,_).
insertInColumn(I,J,[T|Q],B,NewRes):-        insertInBoard(T,I,J,B,TempRes),
                                            TempI is I+1,
                                            insertInColumn(TempI,J,Q,TempRes,NewRes),!.

swapColumn(I,J,B,Res):-                     colToLine(I,B,Res1),
                                            colToLine(J,B,Res2),
                                            insertInColumn(1,I,Res2,B,NewRes1),
                                            insertInColumn(1,J,Res1,NewRes1,Res),!.

% propagation par echange de colonnes de blocs
swapSquare(B,Res):-                         swapColumn(1,9,B,TempRes1),
                                            swapColumn(2,8,TempRes1,TempRes2),
                                            swapColumn(3,7,TempRes2,Res),!.




% read board of variables from files
readBoard(Res,Name):-                       readFile(Boards,Name),normaliseBoardFile(Boards,Res).

normaliseBoardFile([end_of_file],[]).
normaliseBoardFile([T|Q],[T|R]):-           normaliseBoardFile(Q,R),!.

readFile(_):-                          \+ exists_file('Board.txt'),write('Fichier introuvable'),!.%,fail.
%readFile(Boards,Name):-                     open(Name,read,Stream),
%                                            readFile(Stream,Boards,Name),
%                                            close(Stream),nl,!.
%La lecture se fera toujours dans 'Board.txt'
readFile(Board):-open('Board.txt',read,Str),read(Str,Board),close(Str).

%readFile(Stream,[],_):-                     at_end_of_stream(Stream),!.
%readFile(Stream,[T|Q],_):-                  read(Stream,T),
%                                            readFile(Stream,Q,_).

%Ecrire dans un Fichier
ecrire(T) :-
            open('Board.txt', write, Flux),
            write(Flux, T),write(Flux,'.'), nl(Flux),
            close(Flux).

toVar([],_,Out,Out).
toVar([T|Q],Visited,TempOut,Out):-          element(T/Var,Visited),
                                            concat(TempOut,[Var],NewOut),
                                            toVar(Q,Visited,NewOut,Out),!.
toVar([T|Q],Visited,TempOut,Out):-          concat(Visited,[T/Var],NewVisited),
                                            concat(TempOut,[Var],NewOut),
                                            toVar(Q,NewVisited,NewOut,Out),!.


toVarBoard([],[]).
toVarBoard([T|Q],Res):-                     toVar(T,[],[],O),
                                            concat(O,Res,TempRes),
                                            toVarBoard(Q,TempRes).

%Partie gagnée

gameWon(B):- isValidBoard(B), fullGrid(B),nl,write('Bravo! Vous avez fini la grille!'),nl,!.
gameWon(_).
